document.querySelector(".tabs").addEventListener("click", (event) => {
    if (event.target.classList.contains("tabs-title")) {
        document.querySelector(".tabs .active").classList.remove("active");
        event.target.classList.add("active");
        document.querySelector(".tabs-content .active").classList.remove("active");
        document.querySelector(`.tabs-content li[data-nav ="${event.target.dataset.nav}"]`).classList.add("active");
    }
});